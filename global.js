const _ = require('lodash')
const fs = require('fs')
const readFile = (path) => {
    try{
        if(!path) throw(`path is required`)
        let content = fs.readFileSync(path,"utf8")
        return content
    }
    catch(e){
        throw(e)
  }
}
const readJson = _.flow(readFile,x=> JSON.parse(x))

global._ = _
global.readFile = readFile
global.readJson = readJson

